package RegistrationData;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.concurrent.TimeUnit;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class RegisterUsingApachePoi {
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		
		WebDriver driver =new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/register");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		
		File f= new File("/home/saikiran/Documents/DataDriven/testdata2.xlsx");
		FileInputStream fis= new FileInputStream(f);
		XSSFWorkbook workbook=new XSSFWorkbook(fis);
		XSSFSheet sheet=workbook.getSheetAt(1);
		//int rows=sheet.getLastRowNum();
		int rows=sheet.getPhysicalNumberOfRows();
		for(int i=1;i<rows;i++)
		{
			
			
			String firstname=sheet.getRow(i).getCell(0).getStringCellValue();
			String lastname=sheet.getRow(i).getCell(1).getStringCellValue();
			String email=sheet.getRow(i).getCell(2).getStringCellValue();
			String password=sheet.getRow(i).getCell(3).getStringCellValue();
			
			driver.findElement(By.id("FirstName")).sendKeys(firstname);
			driver.findElement(By.id("LastName")).sendKeys(lastname);
			driver.findElement(By.id("Email")).sendKeys(email);
			driver.findElement(By.id("Password")).sendKeys(password);
			driver.findElement(By.id("ConfirmPassword")).sendKeys(password);
			driver.findElement(By.id("register-button")).click();
			System.out.println("Registeration Completed");
		
		}
	}
}

}
