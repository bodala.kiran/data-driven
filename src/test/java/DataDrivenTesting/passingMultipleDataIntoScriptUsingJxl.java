package DataDrivenTesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class passingMultipleDataIntoScriptUsingJxl {

	public static void main(String[] args) throws BiffException, IOException {
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		driver.get("http://demowebshop.tricentis.com/");
		
		File f = new File("/home/saikiran/Documents/DataDriven/testdata1.xls");
		
		FileInputStream fis = new FileInputStream(f);
		Workbook book = Workbook.getWorkbook(fis);
		Sheet sh = book.getSheet("Sheet1");
		
		int rows = sh.getRows();
		int columns = sh.getColumns();
		
		for(int i=0; i<rows;i++) {
			String username = sh.getCell(0,i).getContents();
			String password = sh.getCell(0,i).getContents();
			driver.findElement(By.id("Email")).sendKeys(username);
			driver.findElement(By.id("Password")).sendKeys(password);
			driver.findElement(By.xpath("//input[@value='Log in']")).click();
			
			driver.findElement(By.linkText("Log out")).click();
		}
		

	}

}
